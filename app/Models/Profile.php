<?php

namespace App\Models;

use Carbon\Traits\Date;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Profile
 * @package App\Models
 *
 * @property int id
 * @property int address_id
 * @property int phone_id
 * @property Date date_of_birth
 * @property string avatar
 * @property string cpf
 */
class Profile extends Model
{
    /**
     * Recurso para exclusão lógica
     */
    use SoftDeletes;

    /**
     * Atributos da classe
     * @var array
     */
    protected $fillable = [
        'address_id',
        'phone_id',
        'date_of_birth',
        'avatar',
        'cpf'
    ];

    /**
     * Os atributos que devem serem ocultados.
     * @var array
     */
    protected $hidden = [];

    /**
     * Os atributos que não deve ser atribuido em massa.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Os atributos que devem ser convertidos para tipos nativos.
     *
     * @var array
     */
    protected $casts = [
        'date_of_birth' => 'datetime:Y-m-d',
    ];

    /**
     * Associa o endereço do usuário.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function address()
    {
        return $this->hasOne(Address::class);
    }

    /**
     * Associa o telefone do usuário.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function phone()
    {
        return $this->hasOne(Phone::class);
    }
}
