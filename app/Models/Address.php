<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Address
 * @package App\Models
 *
 * @property int id
 * @property string logradouro
 * @property string description
 * @property string number
 * @property string district
 * @property string city
 * @property string uf
 * @property string longitude
 * @property string latitude
 */
class Address extends Model
{
    use SoftDeletes;

    /**
     * Atributos da classe
     *
     * @var array
     */
    protected $fillable = [
        'logradouro',
        'description',
        'number',
        'district',
        'city',
        'uf',
        'longitude',
        'latitude'
    ];

    /**
     * O que deve ser ocutado ao recuperar o model na base de dados.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Atributos que não deve ser atribuido em massa.
     *
     * @var array
     */
    protected $guarded = [];
}
