<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class EcoRecycler
 * @package App\Models
 *
 * @property User user
 * @property Collection collections
 * @property string declaration
 * @property int quota
 */
class EcoRecycler extends Model
{
    use SoftDeletes;

    /**
     * Atributos da classe
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'declaration',
        'quota'
    ];

    /**
     * O que deve ser ocutado ao recuperar o model na base de dados.
     *
     * @var array
     */
    protected $hidden = [
        'user_id'
    ];

    /**
     * Atributos que não deve ser atribuido em massa.
     *
     * @var array
     */
    protected $guarded = [];

    public function collections()
    {
        return $this->hasMany(Collection::class);
    }
}
