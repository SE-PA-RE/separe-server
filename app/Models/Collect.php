<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Collect
 * @package App\Models
 * @property TypeOfMaterial type
 * @property float weight
 */
class Collect extends Model
{
    use SoftDeletes;

    /**
     * Atributos da classe
     *
     * @var array
     */
    protected $fillable = [
        'type_of_material_id',
        'collection_id',
        'weight'
    ];

    /**
     * O que deve ser ocutado ao recuperar o model na base de dados.
     *
     * @var array
     */
    protected $hidden = [
        'type_of_material_id',
        'collection_id'
    ];

    /**
     * Atributos que não deve ser atribuido em massa.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Tipo do material.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function type()
    {
        return $this->hasOne(TypeOfMaterial::class);
    }
}
