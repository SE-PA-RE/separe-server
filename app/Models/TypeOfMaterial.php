<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TypeOfMaterial
 * @package App\Models
 *
 * @property string type
 * @property string description
 */
class TypeOfMaterial extends Model
{
    use SoftDeletes;

    /**
     * Atributos da classe
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'description'
    ];

    /**
     * O que deve ser ocutado ao recuperar o model na base de dados.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Atributos que não deve ser atribuido em massa.
     *
     * @var array
     */
    protected $guarded = [];
}
