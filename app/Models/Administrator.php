<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Administrator
 * @package App\Models
 *
 * @property User user
 */
class Administrator extends Model
{
    use SoftDeletes;

    /**
     * Atributos da classe
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
    ];

    /**
     * O que deve ser ocutado ao recuperar o model na base de dados.
     *
     * @var array
     */
    protected $hidden = [
        'user_id'
    ];

    /**
     * Atributos que não deve ser atribuido em massa.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Usuário do ADM
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class);
    }
}
