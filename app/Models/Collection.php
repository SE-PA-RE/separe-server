<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Collection
 * @package App\Models
 *
 * @property boolean status
 * @property string image_path
 * @property Collect collects
 * @property Evaluation evaluation
 * @property Address address
 */
class Collection extends Model
{
    use SoftDeletes;

    /**
     * Atributos da classe
     *
     * @var array
     */
    protected $fillable = [
        'recycler_id',
        'friend_id',
        'address_id',
        'evaluation_id',
        'status',
        'image_path'
    ];

    /**
     * O que deve ser ocutado ao recuperar o model na base de dados.
     *
     * @var array
     */
    protected $hidden = [
        'recycler_id',
        'friend_id',
        'address_id',
        'evaluation_id'
    ];

    /**
     * Atributos que não deve ser atribuido em massa.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Avaliação da coleta
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function evaluation()
    {
        return $this->hasOne(Evaluation::class);
    }

    /**
     * Materiais coletados
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function collects()
    {
        return $this->hasMany(Collect::class);
    }

    public function address()
    {
        return $this->hasOne(Address::class);
    }
}
