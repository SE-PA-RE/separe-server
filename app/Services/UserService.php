<?php


namespace App\Services;
use App\Http\Resources\UserResource;
use http\Env\Response;
use Illuminate\Validation\ValidationException;


use App\Repositories\UserRepositoryInterface;

/**
 * Class UserService
 * @package App\Services
 */
class UserService
{
    protected $userRepository;

    /**
     * UserService constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Lista os usuários cadastrado no sistema
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            return $this->userRepository->paginate();
        } catch (\Exception $e) {
            return response()
                ->json(['status'  => false,
                        'return'  => 'NOK',
                        'message' => 'Ocorreu um error no servidor, entre em contato com adminstrador'], 500);
        }
    }

    public function store($request)
    {
        try {

        return response()->json(['status'  => false,
            'return'  => 'NOK',
            'message' => 'Ocorreu um error no servidor, entre em contato com adminstrador']);

        } catch (\Exception $e) {
            return response()
                ->json(['status'  => false,
                    'return'  => 'NOK',
                    'message' => 'Ocorreu um error no servidor, entre em contato com adminstrador'], 500);
        }
    }

    public function show($id)
    {
        try {
            if($id > 0) {
                return response()
                    ->json(['message' => 'ID inválido, id não pode ser menor que 1'], 400);
            }
            return response()
            ->json([new UserResource($this->userRepository->find($id))]);
        } catch (\Exception $e) {
            return response()
                ->json(['status'  => false,
                    'return'  => 'NOK',
                    'message' => 'Ocorreu um error no servidor, entre em contato com adminstrador'], 500);
        }
    }
}
