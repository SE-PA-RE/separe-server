<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collections', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('recycler_id')->nullable();
            $table->unsignedBigInteger('friend_id');
            $table->unsignedBigInteger('address_id');
            $table->unsignedBigInteger('evaluation_id')->nullable();
            $table->enum('status', ['waiting', 'initiated', 'concluded']);
            $table->string('image_path');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('friend_id')->references('id')->on('eco_friends');
            $table->foreign('address_id')->references('id')->on('addresses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collections');
    }
}
