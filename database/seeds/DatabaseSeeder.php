<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(PointOfCollectTableSeeder::class); // Deve ser sempre o primeiro, pois são adicionado os pontos padrões
        $this->call(AdministratorTableSeeder::class); // Deve ser sempre o segundo, adicionar o cadastro do ADM.
        //$this->call(UsersTableSeeder::class);
        //$this->call(AddressTableSeeder::class);
        //$this->call(TypeOfMaterialTableSeeder::class);
    }
}
