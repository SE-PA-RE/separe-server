<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PointOfCollectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Inserindo endereço do ponto de coleta 1
         */
        $point_01 = DB::table('addresses')->insertGetId([
            'logradouro'    => 'Rua NS A',
            'description'   => '301 Norte',
            'number'        => 'Não informado',
            'district'      => 'Plano Diretor Norte',
            'city'          => 'Palmas',
            'state'         => 'Tocantins',
            'uf'            => 'TO',
            'longitude'     => '-10.171058654785156',
            'latitude'      => '-48.33458709716797',
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now()
        ]);

        /**
         * Inserindo endereço do ponto de coleta 2
         */
        $point_02 = DB::table('addresses')->insertGetId([
            'logradouro'    => 'Avenida LO 4',
            'description'   => '104 Norte',
            'number'        => 'Não informado',
            'district'      => 'Plano Diretor Norte',
            'city'          => 'Palmas',
            'state'         => 'Tocantins',
            'uf'            => 'TO',
            'longitude'     => '-10.177135467529297',
            'latitude'      => '-48.328948974609375',
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now()
        ]);

        /**
         * Inserindo endereço do ponto de coleta 3
         */
        $point_03 = DB::table('addresses')->insertGetId([
            'logradouro'    => 'Avenida Nossa Senhora 4',
            'description'   => '104 Sul',
            'number'        => 'Não informado',
            'district'      => 'Plano Diretor Sul',
            'city'          => 'Palmas',
            'state'         => 'Tocantins',
            'uf'            => 'TO',
            'longitude'     => '-10.209824562072754',
            'latitude'      => '-48.323246002197266',
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now()
        ]);

        /**
         * Inserindo endereço do ponto de coleta 4
         */
        $point_04 = DB::table('addresses')->insertGetId([
            'logradouro'    => 'Avenida LO 9',
            'description'   => '403 Sul',
            'number'        => 'Não informado',
            'district'      => 'Plano Diretor Sul',
            'city'          => 'Palmas',
            'state'         => 'Tocantins',
            'uf'            => 'TO',
            'longitude'     => '-10.20461654663086',
            'latitude'      => '-48.338600158691406',
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now()
        ]);

        /**
         * Inserindo ponto de coleta 1
         */
        DB::table('point_of_collects')->insert([
            'address_id'    => $point_01,
            'description'   => 'EcoPonto 1 - Escola Padre Josino Tavares - região Norte.',
            'photo_path'    => null,
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now()
        ]);

        /**
         * Inserindo ponto de coleta 2
         */
        DB::table('point_of_collects')->insert([
            'address_id'    => $point_02,
            'description'   => 'EcoPonto 2 - Parque dos Povos Indígenas - região Norte.',
            'photo_path'    => null,
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now()
        ]);

        /**
         * Inserindo ponto de coleta 2
         */
        DB::table('point_of_collects')->insert([
            'address_id'    => $point_03,
            'description'   => 'EcoPonto 3 - Parque Cesamar - região Sul.',
            'photo_path'    => null,
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now()
        ]);

        /**
         * Inserindo ponto de coleta 3
         */
        DB::table('point_of_collects')->insert([
            'address_id'    => $point_04,
            'description'   => 'EcoPonto 4 - Parque dos Idosos de Palmas - região Sul.',
            'photo_path'    => null,
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now()
        ]);
    }
}
