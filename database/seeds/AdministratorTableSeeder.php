<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class AdministratorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Inserindo endereço do ADM.
         */
        $address_id = DB::table('addresses')->insertGetId([
            'logradouro'    => 'Rua 10',
            'description'   => '',
            'number'        => '100',
            'district'      => 'Oeste',
            'city'          => 'Paraíso do Tocantins',
            'state'         => 'Tocantins',
            'uf'            => 'TO',
            'longitude'     => '',
            'latitude'      => '',
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now()
        ]);

        /**
         * Inserindo telefone do ADM.
         */
        $phone_id = DB::table('phones')->insertGetId([
            'dd'        => '63',
            'number'    => '99279-9584',
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now()
        ]);

        /**
         * Inserindo perfil do ADM.
         */
        $profile_id = DB::table('profiles')->insertGetId([
            'address_id'       => $address_id,
            'phone_id'          => $phone_id,
            'date_of_birth'     => '1990-03-31',
            'cpf'               => '022.404.361-74',
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now()
        ]);

        /**
         * Criando usuário ADM.
         */
        $user_id = DB::table('users')->insertGetId([
            'profile_id'        => $profile_id,
            'name'              => 'Wyllian Fonseca Sales',
            'email'             => 'wylliansalles@gmail.com',
            'password'          => Hash::make('123456'),
            'is_active'         => true,
            'remember_token' => Str::random(10),
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now()
        ]);

        /**
         * Definindo User como ADM.
         */
        DB::table('administrators')->insert([
            'user_id'   => $user_id,
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now()
        ]);
    }
}
