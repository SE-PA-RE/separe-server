<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class TypeOfMaterialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_of_materials')->insert([
            'id'          => 1,
            'type'        => 'Papel',
            'description' => 'Azul',
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now()
        ]);

        DB::table('type_of_materials')->insert([
            'id'          => 2,
            'type'        => 'Plástico',
            'description' => 'Vermelho',
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now()
        ]);

        DB::table('type_of_materials')->insert([
            'type'        => 'Metal',
            'description' => 'Amarelo',
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now()
        ]);

        DB::table('type_of_materials')->insert([
            'type'        => 'Vidro',
            'description' => 'Verde',
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now()
        ]);
    }
}
